const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
const app = express();
const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://uai-catalog.firebaseio.com"
});
app.use(cors({ origin: true}));

const db = admin.firestore();

// app.get('/products');
// app.get('/api/')

app.get('/api/test', (req, resp) => {
  // return resp.status(200).send('roleited');
  // return resp.status(200).send(JSON.stringify(, null, 4));

  // db.child('.info/connected').on('value', connectedSnap => {
  //   if (connectedSnap.val() === true) {
  //     return resp.status(200).send('connected');
  //   } else {
  //     return resp.status(200).send('not connected');
  //   }
  // });
});

app.get('/api/products', async (req, response) => {
  try {
    const product = await db.collection('poc-text').get();
    const resp = product.docs.map(doc => doc.data());
    
    return response.status(200).send(resp);
  } catch (error) {
    console.log('500 - couldnt connect => ', error);
    return response.status(500).send(error);
  }
});

app.post('/api/createProduct', (request, response) => { 
    try {
      db.collection('poc-text').doc(`/${request.body.id}/`).create({
        'active': request.body.active, 
        'description': request.body.description,
        'images': request.body.images,
        'title': request.body.title
      });

      return response.status(200).send();
    } catch (error) {
      console.log('500 - couldnt connect => ', error);
      return response.status(500).send(error);
    }
});
// app.put('/updateProduct');
// app.delete('/deleteProduct/$id');


exports.app = functions.https.onRequest(app);